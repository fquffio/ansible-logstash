Logstash
========

This role's duty is to install [Logstash](https://www.elastic.co/products/logstash).

Variables
---------

### `logstash_version`
The version number of Logstash release to be installed. By default, version **2.2.0** is installed.

### `logstash_checksum`
If set, the downloaded .DEB file is tested against this checksum.

### `logstash_config`
A dictionary of Logstash configuration files. Each item's key represents the config file name, while the value is its content.

### `logstash_world_readable_logs`
Boolean flag to set whether all logs in `/var/log` should be made world-readable. This is particularly useful when using the File input on common logs (e.g. Apache, Redis, …). By default, this option is turned off.

Example
-------

```yaml
---
logstash_config:
  02-input: "{{ lookup('file', playbook_dir + '/files/logstash/02-input.conf') }}"
  10-filter: "{{ lookup('file', playbook_dir + '/files/logstash/02-filter.conf') }}"
  30-output: "{{ lookup('file', playbook_dir + '/files/logstash/02-output.conf') }}"

logstash_world_readable_logs: yes
```
